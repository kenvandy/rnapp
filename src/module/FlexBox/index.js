import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Flex = () => {
  return (
    <View style={styles.container}>
      <View style={[styles.boxSize, styles.boxOne]}>
        <Text>Like</Text>
      </View>

      <View style={[styles.boxSize, styles.boxTwo]}>
        <Text>Comment</Text>
      </View>

      <View style={[styles.boxSize, styles.boxThree]}>
        <Text>Share</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-evenly",
    // alignItems: "center",
  },
  boxSize: {
    width: 60,
    height: 60,
  },
  boxOne: {
    backgroundColor: "rgb(215, 76, 74)",
  },
  boxTwo: {
    backgroundColor: "rgb(234, 171, 86)",
  },
  boxThree: {
    backgroundColor: "rgb(109, 185, 128)",
  },
});

export default Flex;
