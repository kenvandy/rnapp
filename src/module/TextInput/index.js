import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, FlatList } from "react-native";

function MyInput() {
  const [inputValue, setInputValue] = useState("");
  const [todoList, setToDoList] = useState([]);

  function assignToList() {
    setInputValue("");
    setToDoList((prevState) => [...prevState, inputValue]);
  }

  const renderItem = ({ item }) => {
    return (
      <View>
        <Text>{item}</Text>
      </View>
    );
  };

  const renderList = () => {
    return (
      <FlatList
        data={todoList}
        renderItem={renderItem}
        keyExtractor={(item, index) => index}
      />
    );
  };

  const renderInput = () => {
    return (
      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.input}
          onChangeText={(text) => setInputValue(text)}
          onEndEditing={() => assignToList()}
          defaultValue={inputValue}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text>TO DO LIST</Text>
      {renderInput()}
      {renderList()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 40,
  },
  inputWrapper: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: "#bbb",
    marginTop: 20,
    marginBottom: 20,
  },
  input: {
    width: "100%",
    paddingBottom: 10,
  },
});

export default MyInput;
