import React from "react";
import { StyleSheet, Text, View } from "react-native";

function UserManagement() {
  const firstName = "Hien";
  const lastName = "Do";

  let money = 19.5;
  let isMarriaged = true;
  let isLoveYou = false;

  const pi = 3.14;
  const age = 19;

  const add = pi + age;
  const subtract = pi - age;
  const multiply = pi * age;

  const devide = pi / age;

  const district = "ThaiBinh";

  const hienInfo = {
    firstName: "Hien",
    lastName: "Do",
    district: "ThaiBinh",
    age: 30,
    isMarriaged: false,
  };
  const KhanhInfo = {
    firstName: "Khanh",
    lastName: "Ng",
    district: "HaiPhong",
    age: 19,
    isMarriaged: false,
  };

  const myCar = {
    color: "blue",
    brand: "Toyota",
    year: "2011",
  };

  function checkDistrict() {
    if (hienInfo.age < 18) {
      return <Text>Chua duoc dit</Text>;
    } else if (hienInfo.age > 18 || hienInfo.age < 22) {
      return <Text>dit thoai mai di</Text>;
    }
    return <Text>Khong biet gi ca</Text>;
  }

  const myArr = [1, 2, 3, "4", { myName: "Hien" }, true, false];

  const displayName = () => {
    // const fullName = firstName + " " + lastName;
    const fullName = `${firstName} ${lastName}`;
    return <Text>{fullName}</Text>;
  };

  function renderLoop() {
    let myNum = 6;
    myNum++;
    myNum--;
    return <Text style={styles.loopText}>{myNum}</Text>;
  }

  return <View style={styles.container}>{checkDistrict()}</View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  loopText: {
    fontSize: 27,
    color: "red",
  },
});

export default UserManagement;
