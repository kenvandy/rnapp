import React, { useState } from "react";
import { View, Image, Text, StyleSheet, FlatList } from "react-native";

function ImageList({ name }) {
  const [imageName, setImageName] = useState(name);

  const photoList = [
    {
      id: 0,
      name: "Ha Long Bay",
      description: "A beatiful scene taken in Ha Long bay",
      type: "natural",
      url: "https://bit.ly/2kYPEVJ",
    },
    {
      id: 1,
      name: "A cute Cat",
      description: "Hiding from someone is not an easy task",
      type: "animal",
      url: "https://bit.ly/2m2tC4u",
    },
    {
      id: 2,
      name: "Country side",
      description: "This photo reminds me about my childhood",
      type: "natural",
      url: "https://bit.ly/2kW1YWI",
    },
    {
      id: 3,
      name: "Roses",
      description: "A gift for my wife",
      type: "flower",
      url: "https://bit.ly/2kJhncZ",
    },
    {
      id: 4,
      name: "Morning in Yen Bai, Vietnam",
      description: "Rice terraces during daytime",
      type: "natural",
      url: "https://bit.ly/2kMRQQb",
    },

    {
      id: 5,
      name: "My dog",
      description: "He was very happy when he saw his girlfriend",
      type: "animal",
      url: "https://bit.ly/2HbfzAo",
    },
  ];

  // const displayImageItem = () => {
  //   return photoList.map((item, index) => {
  //     return (
  //       <View style={styles.cardWrapper} key={index}>
  //         <Text>{item.name}</Text>
  //         <Image source={{ uri: item.url }} style={styles.photo} />
  //         <Text style={styles.description}>{item.description}</Text>
  //       </View>
  //     );
  //   });
  // };

  const displayImageItem = ({ item }) => {
    return (
      <View style={styles.cardWrapper}>
        <Text>{item.name}</Text>
        <Image source={{ uri: item.url }} style={styles.photo} />
        <Text style={styles.description}>{item.description}</Text>
      </View>
    );
  };

  const renderList = () => {
    return (
      <FlatList
        data={photoList}
        renderItem={displayImageItem}
        keyExtractor={(item) => item.id}
      />
    );
  };

  return <View style={styles.container}>{renderList()}</View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cardWrapper: {
    backgroundColor: "#fff",

    width: "90%",
    borderRadius: 10,
    justifyContent: "center",
    marginBottom: 20,
    padding: 15,

    shadowColor: "#888",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  photo: {
    height: 200,
    marginTop: 15,
    width: "100%",
  },
  description: {
    marginTop: 10,
  },
});

export default ImageList;
