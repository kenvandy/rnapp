import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
} from "react-native-reanimated";

const AnimationTemp = () => {
  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: 100 }],
    };
  });

  function Box() {
    const progress = useSharedValue(0);
    return <Animated.View style={[styles.box, animatedStyle]} />;
  }

  return <View style={styles.container}>{Box()}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  box: {
    width: 80,
    height: 80,
    backgroundColor: "cyan",
  },
});

export default AnimationTemp;
