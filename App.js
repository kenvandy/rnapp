import React from "react";
import FlexDirectionBasics from "./src/module/FlexBox";
import ImageList from "./src/module/ImageList";
import MyInput from "./src/module/TextInput";
import UserManagement from "./src/module/UserManagement";

export default function App() {
  return <MyInput />;
}
